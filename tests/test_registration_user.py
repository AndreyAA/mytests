import allure
from page_object import RegistrationUserPage

@allure.title("Registration new user with correct data")
@allure.severity(allure.severity_level.BLOCKER)
def test_success_registration(browser, host):
    """Registration new user with correct data."""
    page = RegistrationUserPage(browser, host)
    page.open_register_page()
    page.fill_in_the_fields()
    page.click_privacy_checkbox()
    page.click_submit_registration()
    title_after_registration = page.get_title_success_text()
    assert title_after_registration == "Your Account Has Been Created!"

@allure.title("Link to login page is displayed.")
@allure.severity(allure.severity_level.NORMAL)
def test_link_to_login_page(browser, host):
    """Link to login page is displayed."""
    page = RegistrationUserPage(browser, host)
    page.open_register_page()
    link = page.get_link_to_login_page()
    assert link == "https://demo.opencart.com/index.php?route=account/login"


@allure.title("Validation on empty fields in registration")
@allure.severity(allure.severity_level.NORMAL)
def test_registration_with_empty_fields(browser, host):
    """Click submit button when fields has been empty."""
    text_validation = [
        'First Name must be between 1 and 32 characters!',
        'Last Name must be between 1 and 32 characters!',
        'E-Mail Address does not appear to be valid!',
        'Telephone must be between 3 and 32 characters!',
        'Password must be between 4 and 20 characters!']
    page = RegistrationUserPage(browser, host)
    page.open_register_page()
    page.click_submit_registration()

    assert text_validation == page.get_text_validation_on_personal_field()
