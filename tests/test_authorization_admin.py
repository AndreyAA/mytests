import allure

from page_object import AuthorizationAdminPage


@allure.title("Authorization as admin")
@allure.severity(allure.severity_level.BLOCKER)
def test_success_authorization(browser, host):
    """Success authorization."""
    page = AuthorizationAdminPage(browser, host)
    page.open_page()
    page.go_admin_authorization()
    text_button_after_auth = page.get_text_dashboard_button()
    assert text_button_after_auth == "Dashboard"
    allure.dynamic.title("Success authorization as admin")


@allure.title("Authorization as admin with incorrect data")
@allure.severity(allure.severity_level.CRITICAL)
def test_validation_on_auth(browser, host):
    """Validation on authorization page."""
    page = AuthorizationAdminPage(browser, host)
    page.open_page()
    page.go_invalid_data_authorization()
    text = page.get_text_validation()
    assert text == "No match for Username and/or Password.\n×"
