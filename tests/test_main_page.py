import allure
from page_object import MainPage


@allure.title("Check placeholder on search input")
@allure.severity(allure.severity_level.TRIVIAL)
def test_placeholder_value(browser, host):
    """Value of placeholder name."""
    element = MainPage(browser, host)
    element.open_page()
    res = element.get_search_input_placeholder_value()
    assert res == "Search"


@allure.title("Amount of buttons on top navbar")
@allure.severity(allure.severity_level.CRITICAL)
def test_amount_elements_on_navbar(browser, host):
    """Amount of buttons on navbar."""
    element = MainPage(browser, host)
    element.open_page()
    result = element.get_count_elements_on_navbar()
    assert result == 8


@allure.title("Open item in main page")
@allure.severity(allure.severity_level.CRITICAL)
def test_open_element_on_main_page(browser, host):
    """Matches with element displayed on main page and opened element."""
    element = MainPage(browser, host)
    element.open_page()
    title_of_item = element.get_title_element_in_featured_area(3)
    element.click_on_element_in_featured_box(3)
    opened_page_item = element.get_title_opened_item()
    assert title_of_item in opened_page_item


@allure.title("Finding an item using search")
@allure.severity(allure.severity_level.NORMAL)
def test_search_item(browser, host):
    """Enter value in search input."""
    search_value = "iPhone"
    page = MainPage(browser, host)
    page.open_page()
    page.enter_value_in_search_field(search_value)
    page.click_on_search_button()
    title = page.get_title_product_after_search(0)
    assert search_value in title


@allure.title("Open category on top navbar")
@allure.severity(allure.severity_level.CRITICAL)
def test_open_category(browser, host):
    """Open category."""
    page = MainPage(browser, host)
    page.open_page()
    page.click_category_on_navbar(6)
    active_category_on_left_menu = page.get_element_on_left_menu(6)
    assert active_category_on_left_menu == "list-group-item active"


@allure.title("Select currency")
@allure.severity(allure.severity_level.NORMAL)
def test_select_currency(browser, host):
    """Select currency on main page."""
    page = MainPage(browser, host)
    page.open_page()
    current_currency = page.select_currency(1)
    assert current_currency == "GBP"


@allure.title("Add to cart")
@allure.severity(allure.severity_level.CRITICAL)
def test_add_to_cart(browser, host):
    """On main page add item to cart and go to cart."""
    page = MainPage(browser, host)
    page.open_page()
    title_of_1st_element = page.get_title_element_in_featured_area(0)
    title_of_2nd_element = page.get_title_element_in_featured_area(1)
    page.click_element_on_add_to_cart(0)
    page.click_element_on_add_to_cart(1)
    page.click_cart_button_on_main_page()
    title_1st_el_in_cart_after_added = page.get_title_on_popup_cart(0)
    title_2nd_el_in_cart_after_added = page.get_title_on_popup_cart(1)
    assert title_of_1st_element == title_2nd_el_in_cart_after_added
    assert title_of_2nd_element == title_1st_el_in_cart_after_added


@allure.title("Open Privacy Policy on footer")
@allure.severity(allure.severity_level.NORMAL)
def test_open_privacy_policy_page(browser, host):
    """Click on Privacy Policy on footer and open target page"""
    page = MainPage(browser, host)
    page.open_page()
    page.click_privacy_policy_button()
    assert page.get_title_opened_page() == "Privacy Policy"
