import allure
from page_object import AuthorizationUserPage


@allure.title("Login with actual data")
@allure.severity(allure.severity_level.BLOCKER)
def test_success_login(browser, host):
    """Login with actual data."""
    page = AuthorizationUserPage(browser, host)
    page.open_login_page()
    page.user_authorization()
    page.click_logout_button()
    assert "Account Logout" == page.get_title_logout_page()


@allure.title("Warning message after login without data")
@allure.severity(allure.severity_level.NORMAL)
def test_login_without_data(browser, host):
    """Warning message after login without data."""
    page = AuthorizationUserPage(browser, host)
    page.open_login_page()
    page.click_login_button()
    assert page.get_text_dismissible_message() == "Warning: No match for E-Mail Address and/or Password."
