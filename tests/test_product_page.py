import allure
from page_object import ProductPage


@allure.title("Add item to card")
@allure.severity(allure.severity_level.CRITICAL)
def test_add_to_card(browser, host):
    """Open item page and add to card."""
    page = ProductPage(browser, host)
    page.open_product_page(1)
    page.add_to_cart()
    count_text = page.get_count_text_on_cart_button()
    assert '1 item(s)' in count_text


@allure.title("Item was not added to the cart without selecting the required options")
@allure.severity(allure.severity_level.CRITICAL)
def test_incorrect_add_to_card(browser, host):
    """Open item page and try to add without requirement."""
    page = ProductPage(browser, host)
    page.open_product_page()
    page.add_to_cart()
    message = page.get_validation_text()
    assert message == "Select required!"


@allure.title("Add item to favorite")
@allure.severity(allure.severity_level.CRITICAL)
def test_add_to_favorite(browser, host):
    """Add to favorite."""
    page = ProductPage(browser, host)
    page.open_product_page()
    page.add_to_favorite()
    page.click_on_link_to_login_page()
    page.user_authorization()
    page.open_product_page()
    page.add_to_favorite()
    title_count_favorites = page.get_title_count_favorites()
    assert title_count_favorites == "Wish List (1)"


@allure.title("Item without authorization not added to favorites")
@allure.severity(allure.severity_level.NORMAL)
def test_add_to_favorite_without_sign(browser, host):
    """Add to favorite without authorization."""
    page = ProductPage(browser, host)
    page.open_product_page()
    page.add_to_favorite()
    get_link_on_message_to_login = page.get_link_to_login_page()
    title_count_favorites = page.get_title_count_favorites()
    assert title_count_favorites == "Wish List (0)"
    assert get_link_on_message_to_login == "https://demo.opencart.com/index.php?route=account/login"
