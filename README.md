These tests are written for practice in autotests.
Run tests:
1. Install Docker https://docs.docker.com/get-docker/
2. Install Selenoid https://github.com/aerokube/cm/releases

```$ curl -Lo cm https://github.com/aerokube/cm/releases/download/1.2.0/cm_linux_amd64```
```$ chmod +x ./cm```
3. Run Selenoid
```./cm selenoid start```

If you want UI you need add run
```./cm selenoid-ui start```

open http://localhost:8080
4. Go in project, install requirements: ```pip3 install -r requirements.txt``` 
5. Run all tests with vnc option you will use command:

```pytest --browser "<name_browser>" --vnc```
___
For help use (custom options): ```pytest --help```
___
For use allure report use:

https://github.com/allure-framework/allure2/releases/