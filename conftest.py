import pytest
from selenium import webdriver


def pytest_addoption(parser):
    parser.addoption("--browser", action="store", choices=["chrome", "firefox", "opera"],
                     help="Enter browser name")
    parser.addoption("--host", action="store", default="https://demo.opencart.com/",
                     help="Enter HOST for tests as http://localhost/")
    parser.addoption("--executor", action="store", default="localhost", help="ip executor for selenoid")
    parser.addoption("--bversion", action="store", help="Enter browser version as '93.0")
    parser.addoption("--vnc", action="store_true", default=False, help="To see test on running on selenoid")
    parser.addoption("--mobile", action="store_true", help="Run tests on mobile device resolution screen")


@pytest.fixture()
def host(request):
    """"Base url for tests"""
    host = request.config.getoption("--host")
    return host


@pytest.fixture()
def browser(request):
    driver = None
    browser = request.config.getoption("--browser")
    executor = request.config.getoption("--executor")
    executor_url = f"http://{executor}:4444/wd/hub"
    version = request.config.getoption("--bversion")
    vnc = request.config.getoption("--vnc")
    mobile = request.config.getoption("--mobile")
    capabilities = {
        "browserName": browser,
        "browserVersion": version,
        "name": "AndreyAA",
        "selenoid:options": {
            "enableVNC": vnc,
            "screenResolution": "1920x1080"
        },
        'acceptSslCerts': True,
        'acceptInsecureCerts': True,
        'goog:chromeOptions': {
            'args': []
        }
    }
    if browser == "chrome" and mobile:
        capabilities["goog:chromeOptions"]["mobileEmulation"] = {"deviceName": "iPhone X"}
        driver = webdriver.Remote(
            command_executor=executor_url,
            desired_capabilities=capabilities
        )
    else:
        driver = webdriver.Remote(
            command_executor=executor_url,
            desired_capabilities=capabilities
        )

    def fin():
        driver.close()

    request.addfinalizer(fin)

    return driver

