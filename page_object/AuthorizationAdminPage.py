import allure
from .BasePage import BasePage
from .DataForTests import DataForTests
import random
from .locators import LocatorsAuthorizationAdminPage


class AuthorizationAdminPage(BasePage):

    def open_page(self, link: str = "admin/"):
        with allure.step(f"open page {self.url + link}"):
            self.browser.get(self.url + link)

    def go_admin_authorization(self):
        """"Authorization as admin with test data on data.json file"""
        data_for_auth = DataForTests().get_admin_auth_data()
        try:
            self.find_element(LocatorsAuthorizationAdminPage.INPUT_USERNAME).clear()
            self.find_element(LocatorsAuthorizationAdminPage.INPUT_USERNAME).send_keys(data_for_auth["username"])
            self.find_element(LocatorsAuthorizationAdminPage.INPUT_PASSWORD).clear()
            self.find_element(LocatorsAuthorizationAdminPage.INPUT_PASSWORD).send_keys(data_for_auth["password"])
            with allure.step(
                    f"authorization with\nLogin: {data_for_auth['username']}\nPassword: {data_for_auth['password']}"):
                self.find_element(LocatorsAuthorizationAdminPage.SUBMIT_BUTTON).submit()
        except:
            allure.attach(self.browser.get_screenshot_as_png(), name="Screenshot of bug: ",
                          attachment_type=allure.attachment_type.PNG)

    def get_text_dashboard_button(self):
        """"Returned title of first button on navbar panel in admin page"""
        el = self.find_element(LocatorsAuthorizationAdminPage.MENU_DASHBOARD_BUTTON).text
        with allure.step(f"Return text of dashboard button: {el}"):
            return el

    def go_invalid_data_authorization(self):
        """"Authorization as admin with incorrect test data"""
        login = f"abc{random.randint(1, 9)}"
        password = f"abo{random.randint(1, 9)}"
        try:
            self.find_element(LocatorsAuthorizationAdminPage.INPUT_USERNAME).clear()
            self.find_element(LocatorsAuthorizationAdminPage.INPUT_USERNAME).send_keys(login)
            self.find_element(LocatorsAuthorizationAdminPage.INPUT_PASSWORD).clear()
            self.find_element(LocatorsAuthorizationAdminPage.INPUT_PASSWORD).send_keys(password)
            with allure.step(f"authorization with\nLogin: {login}\nPassword: {password}"):
                self.find_element(LocatorsAuthorizationAdminPage.SUBMIT_BUTTON).submit()
        except:
            allure.attach(self.browser.get_screenshot_as_png(), name="Screenshot of bug: ",
                          attachment_type=allure.attachment_type.PNG)

    def get_text_validation(self):
        """"Returned text of notification after invalid authorization"""
        el = self.find_element(LocatorsAuthorizationAdminPage.TEXT_OF_VALIDATION).text
        with allure.step(f"Return text validation: {el}"):
            return el
