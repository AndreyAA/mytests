from .BasePage import BasePage
from .locators import LocatorsAuthorizationUserPage
import allure


class AuthorizationUserPage(BasePage):
    def open_login_page(self, link: str = "index.php?route=account/login"):
        """Open login page."""
        with allure.step(f"open page {self.url + link}"):
            self.browser.get(self.url + link)

    def click_login_button(self):
        """Click on 'login' button."""
        el = self.find_element(LocatorsAuthorizationUserPage.LOGIN_BUTTON)
        self.clickActionChains(el)

    def click_logout_button(self):
        """Click on 'logout' button in account page."""
        el = self.find_element(LocatorsAuthorizationUserPage.LOGOUT_BUTTON)
        self.clickActionChains(el)

    def get_title_logout_page(self):
        """Return text title of logout page."""
        el = self.find_element(LocatorsAuthorizationUserPage.TITLE_LOGOUT_PAGE).text
        with allure.step(f"Return title logout page: {el}"):
            return el

    def get_text_dismissible_message(self):
        """Return text of dismissible message after bad login."""
        el = self.find_element(LocatorsAuthorizationUserPage.DISMISSIBLE_MESSAGE).text
        with allure.step(f"Return text dismissible message: {el}"):
            return el
