import allure
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from .locators import LocatorsAccountPage
from .DataForTests import DataForTests


class BasePage:
    def __init__(self, browser, host):
        """Instance object with driver and base url"""
        self.browser = browser
        self.url = host

    def find_element(self, locator, wait_time=10):
        """Finding an element and waiting for an element"""
        try:
            return WebDriverWait(self.browser, wait_time).until(EC.presence_of_element_located(locator),
                                                                message=f"Can't find element by locator {locator}")
        except:
            allure.attach(self.browser.get_screenshot_as_png(), name="Screenshot of bug: ",
                          attachment_type=allure.attachment_type.PNG)

    def find_elements(self, locator, wait_time=10):
        """Finding items and waiting for those items"""
        try:
            return WebDriverWait(self.browser, wait_time).until(EC.presence_of_all_elements_located(locator),
                                                                message=f"Can't find elements by locator {locator}")
        except:
            allure.attach(self.browser.get_screenshot_as_png(), name="Screenshot of bug: ",
                          attachment_type=allure.attachment_type.PNG)

    def open_page(self, link: str = ''):
        """Open page with base host plus add concatenation other link"""
        with allure.step(f"Open page: {self.url + link}"):
            return self.browser.get(self.url + link)

    def clickActionChains(self, el):
        """Modified click() function"""
        return ActionChains(self.browser).move_to_element(el).click().perform()

    def user_authorization(self):
        """Base authorization with data from file data.json"""
        user_data = DataForTests().get_user_auth_data()
        try:
            self.find_element(LocatorsAccountPage.INPUT_EMAIL).clear()
            self.find_element(LocatorsAccountPage.INPUT_EMAIL).send_keys(user_data["username"])
            self.find_element(LocatorsAccountPage.INPUT_PASSWORD).clear()
            self.find_element(LocatorsAccountPage.INPUT_PASSWORD).send_keys(user_data["password"])
            with allure.step(f"authorization with\nLogin: {user_data['username']}\nPassword: {user_data['password']}"):
                self.find_element(LocatorsAccountPage.SUBMIT_LOGIN_BUTTON).click()
        except:
            allure.attach(self.browser.get_screenshot_as_png(), name="Screenshot of bug: ",
                          attachment_type=allure.attachment_type.PNG)
