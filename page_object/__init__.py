from .DataForTests import DataForTests
from .AuthorizationAdminPage import AuthorizationAdminPage
from .ProductPage import ProductPage
from .MainPage import MainPage
from .RegistrationUserPage import RegistrationUserPage
from .AuthorizationUserPage import AuthorizationUserPage