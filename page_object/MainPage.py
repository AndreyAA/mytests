from selenium.common.exceptions import NoSuchElementException
from .BasePage import BasePage
from .locators import LocatorsMainPage, LocatorsProductPage
import allure


class MainPage(BasePage):

    def get_search_input_placeholder_value(self):
        """Returns the attribute of the found element."""
        el = self.find_element(LocatorsMainPage.SEARCH_INPUT, 1).get_attribute("placeholder")
        with allure.step(f"Return search input placeholder value: {el}"):
            return el

    def get_count_elements_on_navbar(self):
        """Returns amount of buttons in navbar panel"""
        return len(self.find_elements(LocatorsMainPage.ELEMENT_NAVBAR, 1))

    def click_on_element_in_featured_box(self, item_index: int):
        """Clicked on the item that is in the Features block"""
        items = self.find_elements(LocatorsMainPage.FEATURED_PRODUCT)
        return items[item_index].click()

    def get_title_element_in_featured_area(self, item_index: int):
        """Returns title of item in the Features block"""
        description = self.find_elements(LocatorsMainPage.TITLE_FEATURED_PRODUCT)
        with allure.step(f"Return title of item in Features block: {description[item_index].text}"):
            return description[item_index].text

    def get_title_opened_item(self):
        """Returns title of item in item page"""
        description = self.find_element(LocatorsProductPage.TITLE_ON_PRODUCT_PAGE)
        return description.text

    def enter_value_in_search_field(self, value):
        """Put the value in search field"""
        self.find_element(LocatorsMainPage.SEARCH_INPUT).clear()
        with allure.step(f"Searched on main page with test word: {value}"):
            self.find_element(LocatorsMainPage.SEARCH_INPUT).send_keys(str(value))

    def click_on_search_button(self):
        """Click with ActionChains mode in search button"""
        el = self.find_element(LocatorsMainPage.SEARCH_BUTTON)
        self.clickActionChains(el)

    def get_title_product_after_search(self, index: int = 0):
        """"Returned title of founded item after search"""
        title = self.find_elements(LocatorsMainPage.TITLE_IN_LIST_PRODUCTS_AFTER_SEARCH)
        return title[index].text

    def click_category_on_navbar(self, index: int = 0):
        """"Click category in top navbar on index"""
        el = self.find_elements(LocatorsMainPage.ELEMENT_NAVBAR)[index]
        self.clickActionChains(el)

    def get_element_on_left_menu(self, index: int = 0):
        """"Return attribute class on active button after opened page"""
        el = self.find_elements(LocatorsMainPage.MENU_ON_LEFT_SIDE)[index]
        return el.get_attribute("class")

    def select_currency(self, index: int = 0):
        """Select active currency in dropdown menu and return attribute name"""
        el = self.find_element(LocatorsMainPage.SELECT_CURRENCY)
        self.clickActionChains(el)
        rows = self.find_elements(LocatorsMainPage.SELECT_CURRENCY_DROP_DOWN)
        currency = rows[index].get_attribute('name')
        self.clickActionChains(rows[index])
        with allure.step(f"Currency was selected as: {currency}"):
            return currency

    def click_element_on_add_to_cart(self, item_index: int = 0):
        """Click on add to cart in element Featured box"""
        items = self.find_elements(LocatorsMainPage.BUTTON_ADD_TO_CART_FEATURED_PRODUCT)
        with allure.step(f"Click on element: {items[item_index]}"):
            self.clickActionChains(items[item_index])

    def click_cart_button_on_main_page(self):
        """Go to cart from main page. After added item in cart, on cart was created new element in table"""
        try:
            new_element_in_cart = self.find_element(LocatorsMainPage.ELEMENT_CREATE_TABLE_AFTER_ADDED_ITEM)
            if new_element_in_cart.get_attribute('class') == "text-right":
                el = self.find_element(LocatorsMainPage.BUTTON_GO_TO_CART)
                self.clickActionChains(el)
        except NoSuchElementException as e:
            raise e

    def get_title_on_popup_cart(self, item_index: int = 0):
        """Return title added elements in pop-up cart.
        After added item in cart, on cart was created new element in table"""
        try:
            new_element_in_cart = self.find_element(LocatorsMainPage.ELEMENT_CREATE_TABLE_AFTER_ADDED_ITEM)
            if new_element_in_cart.get_attribute('class') == "text-right":
                items = self.find_elements(LocatorsMainPage.TITLE_IN_POPUP_CART)
                with allure.step(f"Return title in pop-up cart: {items[item_index].text}"):
                    return items[item_index].text
        except NoSuchElementException as e:
            raise e

    def click_privacy_policy_button(self):
        """Open Privacy Policy page"""
        try:
            el = self.find_element(LocatorsMainPage.BUTTON_PRIVACY_POLICY)
            with allure.step(f"Click on element: {el.text}"):
                self.clickActionChains(el)
        except NoSuchElementException as e:
            raise e

    def get_title_opened_page(self):
        """Return title opened page"""
        try:
            el = self.find_element(LocatorsMainPage.TITLE_ON_OPEN_PAGE)
            with allure.step(f"Return title opened page: {el.text}"):
                return el.text
        except NoSuchElementException as e:
            raise e