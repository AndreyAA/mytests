import json
import random

"""""
Put the data in file "data.json in folder test_data
Scheme:
{
  "admin":{
    "username": "<DATA>",
    "password": "<DATA>"
  },
  "user": {
    "username": "<DATA>",
    "password": "<DATA>>"
  }
}
"""


class DataForTests:
    def get_admin_auth_data(self):
        """"Use in authorization as admin"""
        with open('./test_data/data.json', 'r') as file:
            data = json.loads(file.read())
            username = data["admin"]["username"]
            password = data["admin"]["password"]
            result = {"username": str(username),
                      "password": str(password)}
            return result

    def get_user_auth_data(self):
        """"Use in authorization as registered user"""
        with open('./test_data/data.json', 'r') as file:
            data = json.loads(file.read())
            username = data["user"]["username"]
            password = data["user"]["password"]
            result = {"username": str(username),
                      "password": str(password)}
            return result

    def register_data_user(self, number_characters: int = 10):
        """"Generate user data to registration"""
        return ''.join(random.choice("1!Aa _h8KkEBbv4#&h") for i in range(number_characters))

    def generate_email(self, number_characters: int = 10):
        res = ''.join(random.choice("1!Aah8KkEBbv4h") for i in range(number_characters))
        return f"{res}@wrvjw.com"
