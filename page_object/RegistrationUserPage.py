from .BasePage import BasePage
from .locators import LocatorsRegistrationUserPage
from .DataForTests import DataForTests
import allure


class RegistrationUserPage(BasePage):
    def open_register_page(self, link: str = "index.php?route=account/register"):
        """Open registration page"""
        with allure.step(f"Open page {self.url + link}"):
            self.browser.get(self.url + link)

    def fill_in_the_fields(self, quantity_of_symbols: int = 10):
        """Fill each requirement fields valid data"""
        fields = self.find_elements(LocatorsRegistrationUserPage.INPUT_REQUIREMENT)
        confirm_password = ""
        with allure.step(f"The fields was fill next data:\n"):
            for el in fields:
                user_data = DataForTests().register_data_user(quantity_of_symbols)
                if el.get_attribute("type") == "email":
                    email = DataForTests().generate_email(quantity_of_symbols)
                    with allure.step(f"Email: {email}"):
                        el.clear()
                        el.send_keys(email)
                elif el.get_attribute("id") == "input-password":
                    password = user_data
                    confirm_password = password
                    with allure.step(f"Password: {password}"):
                        el.clear()
                        el.send_keys(password)
                elif el.get_attribute("name") == "confirm":
                    el.clear()
                    el.send_keys(confirm_password)
                else:
                    el.clear()
                    el.send_keys(user_data)

    def click_privacy_checkbox(self):
        """Click on privacy checkbox in registration page"""
        el = self.find_element(LocatorsRegistrationUserPage.PRIVACY_CHECKBOX)
        self.clickActionChains(el)

    def click_submit_registration(self):
        """Click on submit button"""
        el = self.find_element(LocatorsRegistrationUserPage.SUBMIT_BUTTON_REGISTRATION)
        self.clickActionChains(el)

    def get_text_validation_on_personal_field(self):
        """Return list of danger text on requirement fields"""
        text_fields = self.find_elements(LocatorsRegistrationUserPage.DANGER_TEXT)
        text = []
        for el in text_fields:
            text.append(el.text)
        return text

    def get_link_to_login_page(self):
        """Return link to login page"""
        el = self.find_element(LocatorsRegistrationUserPage.LINK_TO_LOGIN_PAGE)
        link = el.get_attribute("href")
        with allure.step(f"Returned link as: {link}"):
            return link

    def get_title_success_text(self):
        """Return title message after success registration"""
        el = self.find_element(LocatorsRegistrationUserPage.TITLE_AFTER_REGISTRATION).text
        with allure.step(f"Return title message after success registration: {el}"):
            return el
