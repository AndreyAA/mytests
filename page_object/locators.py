from selenium.webdriver.common.by import By

""""Each page separated on class locators."""


class LocatorsMainPage:
    SEARCH_INPUT = (By.XPATH, "//div[@id='search']/input")
    SEARCH_BUTTON = (By.XPATH, "//div[@id='search']//button")
    ELEMENT_NAVBAR = (By.XPATH, "//ul[@class='nav navbar-nav']/li")
    FEATURED_PRODUCT = (By.XPATH, "//div[@id='content']/div[2]/div")
    TITLE_FEATURED_PRODUCT = (By.XPATH, "//div[@id='content']/div[2]/div//h4")
    TITLE_IN_LIST_PRODUCTS_AFTER_SEARCH = (By.XPATH, "//div[@id='content']/div[3]//h4")
    MENU_ON_LEFT_SIDE = (By.XPATH, "//*[@id='column-left']/div[1]/a")
    SELECT_CURRENCY = (By.XPATH, "//*[@id='form-currency']")
    SELECT_CURRENCY_DROP_DOWN = (By.XPATH, "//*[@id='form-currency']/div[1]/ul/li/button")
    BUTTON_ADD_TO_CART_FEATURED_PRODUCT = (By.XPATH, "//div[@class='button-group']/button[1]")
    BUTTON_GO_TO_CART = (By.XPATH, "//div[@id='cart']//button")
    TITLE_IN_POPUP_CART = (By.XPATH, "//*[@id='cart']//td[2]/a")
    ELEMENT_CREATE_TABLE_AFTER_ADDED_ITEM = (By.XPATH, "//*[@id='cart']//tr/td[4]")
    BUTTON_PRIVACY_POLICY = (By.XPATH, "//*[@class='col-sm-3'][1]/ul/li[3]/a")
    TITLE_ON_OPEN_PAGE = (By.XPATH, "//*[@id='content']//h1")


class LocatorsProductPage:
    TITLE_ON_PRODUCT_PAGE = (By.XPATH, "//*[@id='content']//h1")
    AVAILABLE_OPTIONS = (By.XPATH, "//*[@id='input-option226']/option")
    ADD_CARD_BUTTON = (By.XPATH, "//*[@id='button-cart']")
    TITLE_COUNT_FAVORITES_IN_HEADER = (By.XPATH, "//*[@id='wishlist-total']/span")
    TEXT_VALIDATION = (By.XPATH, "//div[@id='product']/div[1]/div")
    BUTTON_ADD_TO_FAVORITE = (By.XPATH, "//*[@id='content']/div/div[2]/div/button[1]")
    LINK_TO_LOGIN_PAGE = (By.XPATH, "//*[@id='product-product']/div[1]/a[1]")
    TEXT_COUNT_ITEM_ON_CARD_BUTTON = (By.XPATH, "//*[@id='cart-total']")


class LocatorsAuthorizationAdminPage:
    INPUT_USERNAME = (By.ID, "input-username")
    INPUT_PASSWORD = (By.ID, "input-password")
    SUBMIT_BUTTON = (By.XPATH, "//button/i")
    MENU_DASHBOARD_BUTTON = (By.ID, "menu-dashboard")
    TEXT_OF_VALIDATION = (By.XPATH, "//div[@class='panel-body']/div")


class LocatorsRegistrationUserPage:
    SUBMIT_BUTTON_REGISTRATION = (By.XPATH, "//input[@type='submit']")
    DANGER_TEXT = (By.XPATH, "//div[@class='text-danger']")
    INPUT_REQUIREMENT = (By.XPATH, "//div[@class='form-group required']/div/input")
    PRIVACY_CHECKBOX = (By.XPATH, "//input[@type='checkbox']")
    TITLE_AFTER_REGISTRATION = (By.XPATH, "//div[@id='content']/h1")
    LINK_TO_LOGIN_PAGE = (By.XPATH, "//*[@id='content']/p/a")

class LocatorsAuthorizationUserPage:
    INPUT_LOGIN_FORM = (By.XPATH, "//div[@class='form-group']/input")
    LOGIN_BUTTON = (By.XPATH, "//input[@type='submit']")
    DISMISSIBLE_MESSAGE = (By.XPATH, "//div[@id='account-login']/div[1]")
    LOGOUT_BUTTON = (By.XPATH, "//*[@id='column-right']//div/a[13]")
    TITLE_LOGOUT_PAGE = (By.XPATH, "//*[@id='content']/h1")


class LocatorsAccountPage:
    INPUT_EMAIL = (By.XPATH, "//*[@id='input-email']")
    INPUT_PASSWORD = (By.XPATH, "//*[@id='input-password']")
    SUBMIT_LOGIN_BUTTON = (By.XPATH, "//*[@id='content']/div/div[2]/div/form/input")
