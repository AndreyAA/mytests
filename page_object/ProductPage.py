from .BasePage import BasePage
from .locators import LocatorsProductPage, LocatorsMainPage, LocatorsAccountPage
import allure


class ProductPage(BasePage):

    def open_product_page(self, number_element: int = 3):
        """Click on item in main page."""
        self.open_page()
        el = self.find_elements(LocatorsMainPage.FEATURED_PRODUCT)
        self.clickActionChains(el[number_element])

    def select_available_options(self, index: int = 0):
        """Select one of available option in item page"""
        el = self.find_elements(LocatorsProductPage.AVAILABLE_OPTIONS)
        self.clickActionChains(el[index])

    def add_to_cart(self):
        """Add to cart item"""
        el = self.find_element(LocatorsProductPage.ADD_CARD_BUTTON)
        self.clickActionChains(el)

    def get_title_count_favorites(self):
        """Returned text of notification after added item in cart"""
        el = self.find_element(LocatorsProductPage.TITLE_COUNT_FAVORITES_IN_HEADER).text
        with allure.step(f"Return success message: {el}"):
            return el

    def get_validation_text(self):
        """Returned text of notification after added item without selected available option"""
        el = self.find_element(LocatorsProductPage.TEXT_VALIDATION).text
        with allure.step(f"Return text warning notification: {el}"):
            return el

    def add_to_favorite(self):
        """Add item to favorite"""
        el = self.find_element(LocatorsProductPage.BUTTON_ADD_TO_FAVORITE)
        self.clickActionChains(el)

    def get_link_to_login_page(self):
        """Returned link to authorization page. After unauthorized action"""
        el = self.find_element(LocatorsProductPage.LINK_TO_LOGIN_PAGE).get_attribute("href")
        with allure.step(f"Return link to login page as: {el}"):
            return el

    def click_on_link_to_login_page(self):
        el = self.find_element(LocatorsProductPage.LINK_TO_LOGIN_PAGE)
        self.clickActionChains(el)

    def get_count_text_on_cart_button(self):
        """Return text count of items on cart button"""
        el = self.find_element(LocatorsProductPage.TEXT_COUNT_ITEM_ON_CARD_BUTTON).text
        with allure.step(f"Return count on cart button: {el}"):
            return el
